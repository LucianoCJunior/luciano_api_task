# Luciano API Task

This Project goal is make a request to the Libring Reporting API and export it's results to a CSV file.

Any gem dependency to this project is listed in Gemfile, so after cloning repository and running the bundler
the application may run successfully.

There's no database or models in this application.

The script to request API is located at app/scripts and the CSV file generated will be saved in this folder as well
