Rails.application.routes.draw do
  root controller: :pages, action: :index
  resources :pages, only: [:index]
  get :run_api, controller: :pages, action: :run_api
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
