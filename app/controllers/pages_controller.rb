class PagesController < ApplicationController
  
  def index
  end
  
  def run_api
    @params = params    
    start_date = Date.strptime(params[:start_date], "%Y-%m-%d").to_s
    end_date = Date.strptime(params[:end_date], "%Y-%m-%d").to_s
    group_by = params[:group_by]    
    if group_by.nil?
      raise 'Invalid Parameters'
    end
    group_by = group_by.join(',')
    #run the Reporting API script
    result = %x(ruby #{Rails.root.join 'app','scripts','api.rb'} #{start_date} #{end_date} #{group_by})
    flash.now[:success] = "File #{File.basename(result)} created successfully"
    render :index
    rescue 
      # rescue if any param is invalid
      flash.now[:danger] = 'Invalid Parameters'
      render :index    
  end
  
end
