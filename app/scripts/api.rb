if ARGV.count >= 3
  
  require 'net/https'
  require 'uri'
  require 'csv'
  require 'json'
  
  # Method defined to make API requests
  def extract_api(uri, token, file)
    https = Net::HTTP.new uri.host, uri.port
    https.use_ssl = true
    request = Net::HTTP::Get.new uri.request_uri, Authorization: "Token #{token}", 'Content-Type' => 'application/json'
    response = https.request request
    json = JSON.parse response.body
    write_headers = !File.exist?(file)
    # If the file do not exist, create a new one and write the params headers
    if !File.exist? file      
      CSV.open(file, 'ab', write_headers: write_headers, headers: ['Start Date', 'End Date', 'Group By', 'Request Time']) do |csv|
        csv << ARGV + [json['request_time']]
      end
    end
    # If the is the first page, write the header
    CSV.open(file, 'ab', write_headers: write_headers, headers: json['connections'].first.keys) do |csv|
      json['connections'].each do |connection|
        csv << connection.values  
      end
    end    
    uri = URI.parse(json['next_page_url'].to_s)
    # Make new requests as many times as number of pages
    if uri.to_s.strip.length > 0
     extract_api uri, token, file 
    end  
  end
  
  start_date = ARGV[0]
  end_date = ARGV[1]
  group_by = ARGV[2]
  
  params = { period: 'custom_date', start_date: start_date, end_date: end_date, group_by: group_by, data_type: 'adnetwork' }  
  token = 'HYCRvSlRAEyRrsHbujvlaoRPf'
  
  uri = URI.parse('https://api.libring.com/v2/reporting/get')
  uri.query = URI.encode_www_form(params)
  
  file = File.join(File.dirname(__FILE__),"API#{Time.now.strftime "%Y%m%d%H%M%S"}.csv")
  
  puts file 
  
  extract_api uri, token, file  
  
end
